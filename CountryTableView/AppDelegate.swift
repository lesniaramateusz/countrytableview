//
//  AppDelegate.swift
//  CountryTableView
//
//  Created by Mateusz Leśniara on 11/06/2020.
//  Copyright © 2020 Mateusz Leśniara. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

