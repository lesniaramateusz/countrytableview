//
//  Country.swift
//  CountryTableView
//
//  Created by Mateusz Leśniara on 21/06/2020.
//  Copyright © 2020 Mateusz Leśniara. All rights reserved.
//

import Foundation

class Country: Codable {
    var name: String?
    var capital: String?
    var countryCode:String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case capital
        case countryCode = "alpha3Code"
    }
}

