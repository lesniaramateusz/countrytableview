//
//  CountryPresenter.swift
//  CountryTableView
//
//  Created by Mateusz Leśniara on 11/06/2020.
//  Copyright © 2020 Mateusz Leśniara. All rights reserved.
//

import Foundation
protocol CountryView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setCountry(country: [CountryViewData])
    func setEmptyCountry()
}
struct CountryViewData{
    let countryName: String
    let capitalName: String
}

class CountryPresenter {
    private let countryService:CountryService
    
    weak private var countryView: CountryView?
    init(countryService:CountryService) {
        self.countryService = countryService
    }
    
    func attachView(view:CountryView) {
        countryView = view
    }
    
    func detachView() {
        countryView = nil
    }
    
    func getCountries() {
        self.countryView?.startLoading()
        countryService.callAPIGetCountries(   onSuccess: { (country) in
            self.countryView?.finishLoading()
            if (country.isEmpty){
                self.countryView?.setEmptyCountry()
            } else {
                let mappedCountries = country.map {
                    return CountryViewData(countryName: "\($0.name!)", capitalName: "\($0.capital!)")
                }
                self.countryView?.setCountry(country: mappedCountries)
            }
        },
                                              onFailure: { (errorMessage) in
                                                self.countryView?.finishLoading()
        }
        )
    }
    
}
