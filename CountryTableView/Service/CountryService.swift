//
//  CountryService.swift
//  CountryTableView
//
//  Created by Mateusz Leśniara on 11/06/2020.
//  Copyright © 2020 Mateusz Leśniara. All rights reserved.
//

import Foundation

class CountryService {
    public func callAPIGetCountries(onSuccess successCallback: ((_ country: [Country]) -> Void)?,
                                    onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        ServiceManager.instance.callAPIGetCountries(
            onSuccess: { (country) in
                successCallback?(country)
        },
            onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
        }
        )
    }
}
