//
//  ServiceManager.swift
//  CountryTableView
//
//  Created by Mateusz Leśniara on 21/06/2020.
//  Copyright © 2020 Mateusz Leśniara. All rights reserved.
//

import Foundation
import Alamofire



class ServiceManager {
    static let instance = ServiceManager()
    private enum Constants {
        static let baseUrl = "https://restcountries.eu/rest/v2"
    }
    
    enum RequestMethod {
        case get
    }
    
    enum Endpoint: String {
        case allCountries = "/all"
    }
    
    func callAPIGetCountries(onSuccess successCallback: ((_ country: [Country]) -> Void)?,
                             onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        let url = Constants.baseUrl + Endpoint.allCountries.rawValue
        
        self.createRequest(
            url, method: .get, headers: nil, parameters: nil,
            onSuccess: { responseData -> Void in
                
                if let data = responseData {
                    let decoder = JSONDecoder()
                    let countries = try? decoder.decode([Country].self, from: data)
                        successCallback?(countries ?? [])
                } else {
                    failureCallback?("An error has occured.")
                }
        },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
        }
        )
    }
    
    
    func createRequest(
        _ url: String,
        method: HTTPMethod,
        headers: [String: String]?,
        parameters: AnyObject?,
        onSuccess successCallback: ((Data?) -> Void)?,
        onFailure failureCallback: ((String) -> Void)?
    ) {
        
        AF.request(url, method: method).validate().response{ response in
            switch response.result {
            case .success(let value):
                successCallback?(value)
            case .failure(let error):
                if let callback = failureCallback {
                    callback(error.localizedDescription)
                }
            }
        }
    }
}
