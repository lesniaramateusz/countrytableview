//
//  CountryViewController.swift
//  CountryTableView
//
//  Created by Mateusz Leśniara on 22/06/2020.
//  Copyright © 2020 Mateusz Leśniara. All rights reserved.
//

import UIKit


class CountryTableViewCell: UITableViewCell {
    @IBOutlet weak var countryNameLabel: UILabel!
 
    @IBOutlet weak var capitalNameLabel: UILabel!
}

class CountryViewController: UIViewController {
  
  
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var countryList: UITableView!
    
    let presenter = CountryPresenter(countryService: CountryService())
    var countryToDisplay = [CountryViewData]()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryList?.dataSource = self
        loader.hidesWhenStopped = true
        presenter.attachView(view: self)
        presenter.getCountries()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension CountryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countryToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as! CountryTableViewCell
        let countryViewData = countryToDisplay[indexPath.row]
        cell.countryNameLabel?.text = countryViewData.countryName
        cell.capitalNameLabel?.text = countryViewData.capitalName
        return cell
    }
}

extension CountryViewController: CountryView {
    func startLoading() {
        loader?.startAnimating()
    }
    
    func finishLoading() {
        loader?.stopAnimating()
    }
    
    func setCountry(country: [CountryViewData]) {
        countryToDisplay = country
        countryList?.isHidden = false
        countryList?.reloadData()
    }
    
    func setEmptyCountry() {
        countryList?.isHidden = false
    }
}
